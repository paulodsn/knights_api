import {
  ConstructorArgs,
  KnightModel,
} from 'src/modules/knight/models/knight.model';
import { WeaponDataBuilder } from './WeaponDataBuilder';
import { CharacterDataBuilder } from './CharacterDataBuilder';

export class KnightDataBuilder {
  #knight: ConstructorArgs;

  constructor() {
    const weapon = WeaponDataBuilder.aWeapon().build();
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const { weaponIds, ...character } =
      CharacterDataBuilder.aCharacter().build();

    this.#knight = {
      ...character,
      weapons: [weapon],
    };
  }

  static aKnight(): KnightDataBuilder {
    return new KnightDataBuilder();
  }

  build() {
    return new KnightModel(this.#knight);
  }
}
