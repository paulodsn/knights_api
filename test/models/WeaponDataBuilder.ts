import { WeaponModel } from 'src/modules/weapon/models/weapon.model';

export class WeaponDataBuilder {
  #weapon: WeaponModel;

  constructor() {
    this.#weapon = new WeaponModel({
      id: '123',
      attr: 'strength',
      equipped: true,
      mod: 3,
      name: 'sword',
    });
  }

  static aWeapon(): WeaponDataBuilder {
    return new WeaponDataBuilder();
  }

  build() {
    return new WeaponModel(this.#weapon);
  }
}
