import { CharacterModel } from 'src/modules/character/models/character.model';

export class CharacterDataBuilder {
  #knight: CharacterModel;

  constructor() {
    this.#knight = new CharacterModel({
      id: '123',
      birthday: new Date('1970-01-01'),
      keyAttribute: 'strength',
      name: 'Lancelot',
      nickname: 'Lance',
      weaponIds: [],
      attributes: {
        strength: 0,
        dexterity: 0,
        constitution: 0,
        intelligence: 0,
        wisdom: 0,
        charisma: 0,
      },
      hero: true,
    });
  }

  static aCharacter(): CharacterDataBuilder {
    return new CharacterDataBuilder();
  }

  build() {
    return new CharacterModel(this.#knight);
  }
}
