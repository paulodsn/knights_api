import { PrismaClient } from '@prisma/client';

export const charactersSeed = async (prisma: PrismaClient) => {
  const weapons = await prisma.weapon.findMany();

  const getRandomWeapon = () => {
    const min = 0;
    const max = weapons.length - 1;

    const randomNumber = Math.floor(Math.random() * (max - min + 1) + min);
    return [weapons[randomNumber].id];
  };

  return prisma.character.createMany({
    data: [
      {
        name: 'Knight',
        attributes: {
          strength: 10,
          dexterity: 5,
          intelligence: 2,
          charisma: 3,
          constitution: 10,
          wisdom: 2,
        },
        keyAttribute: 'strength',
        birthday: new Date('1900-01-01'),
        nickname: 'The Knight',
        weaponIds: getRandomWeapon(),
        hero: true,
      },
      {
        name: 'Archer',
        attributes: {
          strength: 5,
          dexterity: 10,
          intelligence: 2,
          charisma: 3,
          constitution: 5,
          wisdom: 2,
        },
        keyAttribute: 'dexterity',
        birthday: new Date('1950-01-01'),
        nickname: 'The Archer',
        weaponIds: getRandomWeapon(),
        hero: true,
      },
      {
        name: 'Mage',
        attributes: {
          strength: 2,
          dexterity: 5,
          intelligence: 10,
          charisma: 3,
          constitution: 5,
          wisdom: 10,
        },
        keyAttribute: 'intelligence',
        birthday: new Date('2021-01-01'),
        nickname: 'The Mage',
        weaponIds: getRandomWeapon(),
      },
    ],
  });
};
