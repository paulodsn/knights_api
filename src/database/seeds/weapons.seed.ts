import { PrismaClient } from '@prisma/client';

export const weaponsSeed = async (prisma: PrismaClient) => {
  return prisma.weapon.createMany({
    data: [
      {
        name: 'Sword',
        attr: 'strength',
        equipped: true,
        mod: 1.5,
      },
      {
        name: 'Axe',
        attr: 'strength',
        equipped: true,
        mod: 2,
      },
      {
        name: 'Bow',
        attr: 'dexterity',
        equipped: true,
        mod: 2,
      },
      {
        name: 'Staff',
        attr: 'intelligence',
        equipped: true,
        mod: 2,
      },
    ],
  });
};
