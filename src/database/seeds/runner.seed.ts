import { PrismaClient } from '@prisma/client';
import { weaponsSeed } from './weapons.seed';
import { charactersSeed } from './characters.seed';

const prisma = new PrismaClient();

const main = async () => {
  const seeds = [weaponsSeed, charactersSeed];

  for (const seed of seeds) {
    await seed(prisma);
  }
};

main()
  .then(async () => {
    await prisma.$disconnect();
  })
  .catch(async (e) => {
    console.error(e);
    await prisma.$disconnect();
    process.exit(1);
  });
