import { Module } from '@nestjs/common';
import { KnightModule } from './modules/knight/knight.module';
import { CharacterModule } from './modules/character/character.module';
import { WeaponModule } from './modules/weapon/weapon.module';

@Module({
  imports: [KnightModule, CharacterModule, WeaponModule],
  controllers: [],
  providers: [],
})
export class AppModule {}
