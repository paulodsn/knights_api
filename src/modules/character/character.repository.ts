import { PrismaService } from 'src/database/prisma.service';
import { CharacterModel } from './models/character.model';
import { Injectable } from '@nestjs/common';
import { Prisma } from '@prisma/client';

@Injectable()
export class CharacterRepository {
  constructor(private readonly prismaService: PrismaService) {}

  async list(where?: Prisma.CharacterWhereInput) {
    const characters = await this.prismaService.character.findMany({
      where,
    });
    return characters.map((character) => new CharacterModel(character));
  }

  async create(data: Prisma.CharacterUncheckedCreateInput) {
    const character = await this.prismaService.character.create({ data });
    return new CharacterModel(character);
  }

  async get(id: string) {
    const character = await this.prismaService.character.findUniqueOrThrow({
      where: { id },
    });

    return new CharacterModel(character);
  }

  async update(id: string, data: Prisma.CharacterUncheckedUpdateInput) {
    const character = await this.prismaService.character.update({
      where: { id },
      data,
    });

    return new CharacterModel(character);
  }

  async delete(id: string) {
    await this.prismaService.character.delete({
      where: { id },
    });
  }
}
