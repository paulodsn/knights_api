import { Module } from '@nestjs/common';
import { CharacterRepository } from './character.repository';
import { PrismaModule } from 'src/database/prisma.module';

@Module({
  providers: [CharacterRepository],
  imports: [PrismaModule],
  exports: [CharacterRepository],
})
export class CharacterModule {}
