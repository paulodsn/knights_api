import { PrismaModule } from 'src/database/prisma.module';
import { CharacterRepository } from './character.repository';
import { Test, TestingModule } from '@nestjs/testing';
import { DeepMockProxy, mockDeep } from 'jest-mock-extended';
import { PrismaService } from 'src/database/prisma.service';
import { PrismaClient } from '@prisma/client';
import { CharacterDataBuilder } from 'test/models/CharacterDataBuilder';

describe('WeaponRepository', () => {
  let repository: CharacterRepository;
  let prisma: DeepMockProxy<PrismaClient>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [CharacterRepository],
      imports: [PrismaModule],
    })
      .overrideProvider(PrismaService)
      .useValue(mockDeep<PrismaClient>())
      .compile();

    repository = module.get<CharacterRepository>(CharacterRepository);
    prisma = module.get(PrismaService);
  });

  it('should be defined', () => {
    expect(repository).toBeDefined();
  });

  it('should return an array of characters', async () => {
    const characters = [CharacterDataBuilder.aCharacter().build()];
    prisma.character.findMany.mockResolvedValueOnce(characters);

    const result = await repository.list();

    expect(result).toEqual(characters);
  });

  it('should create a new character', async () => {
    const character = CharacterDataBuilder.aCharacter().build();
    prisma.character.create.mockResolvedValueOnce(character);

    const result = await repository.create(character);

    expect(result).toEqual(character);
  });

  it('should return a character', async () => {
    const character = CharacterDataBuilder.aCharacter().build();
    prisma.character.findUniqueOrThrow.mockResolvedValueOnce(character);

    const result = await repository.get(character.id);

    expect(result).toEqual(character);
  });

  it('should update a character', async () => {
    const character = CharacterDataBuilder.aCharacter().build();
    prisma.character.update.mockResolvedValueOnce(character);

    const result = await repository.update(character.id, character);

    expect(result).toEqual(character);
  });

  it('should delete a character', async () => {
    const character = CharacterDataBuilder.aCharacter().build();
    prisma.character.delete.mockResolvedValueOnce(character);

    await repository.delete(character.id);

    expect(prisma.character.delete).toHaveBeenCalledWith({
      where: { id: character.id },
    });
  });
});
