export class CharacterModel {
  id: string;
  name: string;
  nickname: string | null;
  birthday: Date | null;
  weaponIds: string[];
  attributes: {
    strength: number;
    dexterity: number;
    constitution: number;
    intelligence: number;
    wisdom: number;
    charisma: number;
  };
  keyAttribute: string;
  hero: boolean;

  constructor(data: CharacterModel) {
    Object.assign(this, data);
  }
}
