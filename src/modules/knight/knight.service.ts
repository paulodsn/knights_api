import { Injectable } from '@nestjs/common';
import { CharacterRepository } from '../character/character.repository';
import { WeaponRepository } from '../weapon/weapon.repository';
import { KnightModel } from './models/knight.model';
import { CreateKnightDto } from './dtos/create-knigth.dto';
import { UpdateKnightDto } from './dtos/update-knigth.dto';

@Injectable()
export class KnightService {
  constructor(
    private readonly characterRepository: CharacterRepository,
    private readonly weaponRepository: WeaponRepository,
  ) {}

  async list(filter?: string) {
    const characters = await this.characterRepository.list({
      hero: filter === 'heroes' ? true : undefined,
    });

    const knights = await Promise.all(
      characters.map(async ({ weaponIds, ...knight }) => {
        const weapons = await this.weaponRepository.findMany(weaponIds);
        return new KnightModel({ ...knight, weapons });
      }),
    );

    return knights;
  }

  async create(knightDto: CreateKnightDto) {
    const { weaponIds, ...character } = knightDto;

    const createdCharacter = await this.characterRepository.create({
      weaponIds,
      ...character,
    });

    const weapons = await this.weaponRepository.findMany(weaponIds);
    return new KnightModel({ ...createdCharacter, weapons });
  }

  async get(id: string) {
    const character = await this.characterRepository.get(id);
    const weapons = await this.weaponRepository.findMany(character.weaponIds);
    return new KnightModel({ ...character, weapons });
  }

  async update(id: string, knightDto: UpdateKnightDto) {
    const { weaponIds, ...character } = knightDto;
    const updatedCharacter = await this.characterRepository.update(id, {
      ...character,
      weaponIds,
    });

    const weapons = await this.weaponRepository.findMany(weaponIds);
    return new KnightModel({ ...updatedCharacter, weapons });
  }

  async delete(id: string) {
    await this.characterRepository.delete(id);
  }
}
