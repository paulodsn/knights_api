import { CharacterModel } from 'src/modules/character/models/character.model';
import { WeaponModel } from 'src/modules/weapon/models/weapon.model';
import * as dayjs from 'dayjs';

type omitFields = 'mod' | 'atack' | 'age' | 'toJSON' | 'experience';
export type ConstructorArgs = Omit<KnightModel, omitFields>;

export class KnightModel implements Omit<CharacterModel, 'weaponIds'> {
  id: string;
  name: string;
  nickname: string | null;
  birthday: Date | null;
  weapons: WeaponModel[];
  attributes: {
    strength: number;
    dexterity: number;
    constitution: number;
    intelligence: number;
    wisdom: number;
    charisma: number;
  };
  keyAttribute: string;
  hero: boolean;

  constructor(data: ConstructorArgs) {
    this.id = data.id;
    this.name = data.name;
    this.nickname = data.nickname;
    this.birthday = data.birthday;
    this.weapons = data.weapons;
    this.attributes = data.attributes;
    this.keyAttribute = data.keyAttribute;
    this.hero = data.hero;
  }

  public get mod() {
    const value = this.attributes[this.keyAttribute];
    if (value <= 8) {
      return -2;
    } else if (value <= 10) {
      return -1;
    } else if (value <= 12) {
      return 0;
    } else if (value <= 15) {
      return 1;
    } else if (value <= 18) {
      return 2;
    } else {
      return 3;
    }
  }

  public get age() {
    return dayjs().diff(this.birthday, 'year');
  }

  public get atack() {
    return (
      10 + this.mod + this.weapons.reduce((acc, weapon) => acc + weapon.mod, 0)
    );
  }

  public get experience() {
    if (this.age < 7) {
      return 0;
    }

    return Math.floor((this.age - 7) * Math.pow(22, 1.45));
  }

  toJSON() {
    return {
      id: this.id,
      name: this.name,
      nickname: this.nickname,
      birthday: dayjs(this.birthday).format('YYYY-MM-DD'),
      weapons: this.weapons,
      attributes: this.attributes,
      keyAttribute: this.keyAttribute,
      atack: this.atack,
      age: this.age,
      experience: this.experience,
    };
  }
}
