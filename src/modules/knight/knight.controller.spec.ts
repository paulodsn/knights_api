import { Test, TestingModule } from '@nestjs/testing';
import { KnightController } from './knight.controller';
import { KnightDataBuilder } from 'test/models/KnightDataBuilder';
import { KnightService } from './knight.service';
import { CharacterModule } from '../character/character.module';
import { WeaponModule } from '../weapon/weapon.module';
import { PrismaModule } from 'src/database/prisma.module';

describe('KnightController', () => {
  let controller: KnightController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [KnightController],
      providers: [KnightService],
      imports: [CharacterModule, WeaponModule, PrismaModule],
    }).compile();

    controller = module.get<KnightController>(KnightController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should return an array of knights', async () => {
    const knights = [KnightDataBuilder.aKnight().build()];
    jest.spyOn(KnightService.prototype, 'list').mockResolvedValueOnce(knights);

    const result = await controller.list();

    expect(result).toEqual(knights);
  });

  it('should create a new knight', async () => {
    const knight = KnightDataBuilder.aKnight().build();
    jest.spyOn(KnightService.prototype, 'create').mockResolvedValueOnce(knight);

    const result = await controller.create({
      name: 'Sir Lancelot',
      nickname: 'Lance',
      birthday: new Date(),
      weaponIds: ['1'],
      attributes: {
        strength: 10,
        dexterity: 10,
        constitution: 10,
        intelligence: 10,
        wisdom: 10,
        charisma: 10,
      },
      keyAttribute: 'strength',
    });

    expect(result).toEqual(knight);
  });

  it('should return a knight by id', async () => {
    const knight = KnightDataBuilder.aKnight().build();
    jest.spyOn(KnightService.prototype, 'get').mockResolvedValueOnce(knight);

    const result = await controller.get('1');

    expect(result).toEqual(knight);
  });

  it('should update a knight', async () => {
    const knight = KnightDataBuilder.aKnight().build();
    jest.spyOn(KnightService.prototype, 'update').mockResolvedValueOnce(knight);

    const result = await controller.update('1', {
      name: 'Sir Lancelot',
      nickname: 'Lance',
      birthday: new Date(),
      weaponIds: ['1'],
      attributes: {
        strength: 10,
        dexterity: 10,
        constitution: 10,
        intelligence: 10,
        wisdom: 10,
        charisma: 10,
      },
      keyAttribute: 'strength',
    });

    expect(result).toEqual(knight);
  });

  it('should delete a knight', async () => {
    jest
      .spyOn(KnightService.prototype, 'delete')
      .mockResolvedValueOnce(undefined);

    const result = await controller.delete('1');

    expect(result).toBeUndefined();
  });
});
