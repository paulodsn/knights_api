import { Test, TestingModule } from '@nestjs/testing';
import { KnightService } from './knight.service';
import { CharacterModule } from '../character/character.module';
import { WeaponModule } from '../weapon/weapon.module';
import { PrismaModule } from 'src/database/prisma.module';
import { CharacterDataBuilder } from 'test/models/CharacterDataBuilder';
import { WeaponDataBuilder } from 'test/models/WeaponDataBuilder';
import { CharacterRepository } from '../character/character.repository';
import { WeaponRepository } from '../weapon/weapon.repository';
import { KnightDataBuilder } from 'test/models/KnightDataBuilder';

describe('KnightService', () => {
  let service: KnightService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [KnightService],
      imports: [CharacterModule, WeaponModule, PrismaModule],
    }).compile();

    service = module.get<KnightService>(KnightService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should return an array of knights', async () => {
    const characters = [CharacterDataBuilder.aCharacter().build()];
    const weapon = WeaponDataBuilder.aWeapon().build();
    const knight = [KnightDataBuilder.aKnight().build()];

    jest
      .spyOn(CharacterRepository.prototype, 'list')
      .mockResolvedValue(characters);

    jest
      .spyOn(WeaponRepository.prototype, 'findMany')
      .mockResolvedValue([weapon]);

    const result = await service.list();

    expect(result).toEqual(knight);
  });

  it('should return an array of heroes', async () => {
    const characters = [CharacterDataBuilder.aCharacter().build()];
    const weapon = WeaponDataBuilder.aWeapon().build();
    const knight = [KnightDataBuilder.aKnight().build()];

    jest
      .spyOn(CharacterRepository.prototype, 'list')
      .mockResolvedValue(characters);

    jest
      .spyOn(WeaponRepository.prototype, 'findMany')
      .mockResolvedValue([weapon]);

    const result = await service.list('heroes');

    expect(result).toEqual(knight);
  });

  it('should create a new knight', async () => {
    const knight = KnightDataBuilder.aKnight().build();
    const characters = CharacterDataBuilder.aCharacter().build();
    const weapon = WeaponDataBuilder.aWeapon().build();

    jest
      .spyOn(CharacterRepository.prototype, 'create')
      .mockResolvedValue(characters);

    jest.spyOn(WeaponRepository.prototype, 'create').mockResolvedValue(weapon);

    const result = await service.create({
      name: 'Lancelot',
      nickname: 'Lance',
      birthday: new Date('1970-01-01'),
      weaponIds: ['1'],
      attributes: {
        strength: 0,
        dexterity: 0,
        constitution: 0,
        intelligence: 0,
        wisdom: 0,
        charisma: 0,
      },
      keyAttribute: 'strength',
    });

    expect(result).toEqual(knight);
  });

  it('should return a knight by id', async () => {
    const knight = KnightDataBuilder.aKnight().build();
    const character = CharacterDataBuilder.aCharacter().build();
    const weapon = WeaponDataBuilder.aWeapon().build();

    jest
      .spyOn(CharacterRepository.prototype, 'get')
      .mockResolvedValue(character);

    jest
      .spyOn(WeaponRepository.prototype, 'findMany')
      .mockResolvedValue([weapon]);

    const result = await service.get('1');

    expect(result).toEqual(knight);
  });

  it('should update a knight', async () => {
    const knight = KnightDataBuilder.aKnight().build();
    const character = CharacterDataBuilder.aCharacter().build();
    const weapon = WeaponDataBuilder.aWeapon().build();

    jest
      .spyOn(CharacterRepository.prototype, 'update')
      .mockResolvedValue(character);

    jest
      .spyOn(WeaponRepository.prototype, 'findMany')
      .mockResolvedValue([weapon]);

    const result = await service.update('1', {
      name: 'Lancelot',
      nickname: 'Lance',
      birthday: new Date('1970-01-01'),
      weaponIds: ['1'],
      attributes: {
        strength: 0,
        dexterity: 0,
        constitution: 0,
        intelligence: 0,
        wisdom: 0,
        charisma: 0,
      },
      keyAttribute: 'strength',
    });

    expect(result).toEqual(knight);
  });

  it('should delete a knight', async () => {
    jest.spyOn(CharacterRepository.prototype, 'delete').mockResolvedValue();
    await service.delete('1');
    expect(CharacterRepository.prototype.delete).toBeCalledWith('1');
  });
});
