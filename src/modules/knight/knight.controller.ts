import {
  Body,
  ClassSerializerInterceptor,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  Query,
  UseInterceptors,
  ValidationPipe,
} from '@nestjs/common';
import { ApiResponse, ApiTags } from '@nestjs/swagger';
import { KnightService } from './knight.service';
import { CreateKnightDto } from './dtos/create-knigth.dto';
import { UpdateKnightDto } from './dtos/update-knigth.dto';

@Controller('knights')
export class KnightController {
  constructor(private readonly knightService: KnightService) {}

  @Get()
  @ApiTags('knights')
  @ApiResponse({ status: 200, description: 'List' })
  @ApiResponse({ status: 400, description: 'Bad request' })
  async list(@Query('filter') filter?: string) {
    const knights = await this.knightService.list(filter);
    return knights;
  }

  @Post()
  @ApiTags('knights')
  @ApiResponse({ status: 201, description: 'Create a new knight' })
  @ApiResponse({ status: 400, description: 'Bad request' })
  @UseInterceptors(ClassSerializerInterceptor)
  async create(
    @Body(new ValidationPipe({ transform: true }))
    createKnightDto: CreateKnightDto,
  ) {
    const knight = await this.knightService.create(createKnightDto);
    return knight;
  }

  @Get(':id')
  @ApiTags('knights')
  @ApiResponse({ status: 200, description: 'Get a knight' })
  @ApiResponse({ status: 400, description: 'Bad request' })
  async get(@Param('id') id: string) {
    const knight = await this.knightService.get(id);
    return knight;
  }

  @Patch(':id')
  @ApiTags('knights')
  @ApiResponse({ status: 200, description: 'Update a knight' })
  @ApiResponse({ status: 400, description: 'Bad request' })
  async update(
    @Param('id') id: string,
    @Body(new ValidationPipe({ transform: true }))
    updateKnightDto: UpdateKnightDto,
  ) {
    const knight = await this.knightService.update(id, updateKnightDto);
    return knight;
  }

  @Delete(':id')
  @ApiTags('knights')
  @ApiResponse({ status: 200, description: 'Delete a knight' })
  @ApiResponse({ status: 400, description: 'Bad request' })
  async delete(@Param('id') id: string) {
    await this.knightService.delete(id);
  }
}
