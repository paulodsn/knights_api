import { Module } from '@nestjs/common';
import { KnightController } from './knight.controller';
import { KnightService } from './knight.service';
import { WeaponModule } from '../weapon/weapon.module';
import { PrismaModule } from 'src/database/prisma.module';
import { CharacterModule } from '../character/character.module';

@Module({
  controllers: [KnightController],
  providers: [KnightService],
  imports: [CharacterModule, WeaponModule, PrismaModule],
})
export class KnightModule {}
