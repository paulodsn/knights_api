import { Transform } from 'class-transformer';
import {
  IsArray,
  IsDate,
  IsNumber,
  IsString,
  ValidateNested,
} from 'class-validator';

class Attributes {
  @IsNumber()
  strength: number;
  @IsNumber()
  dexterity: number;
  @IsNumber()
  constitution: number;
  @IsNumber()
  intelligence: number;
  @IsNumber()
  wisdom: number;
  @IsNumber()
  charisma: number;
}

export class CreateKnightDto {
  @IsString()
  name: string;
  @IsString()
  nickname: string;
  @IsDate()
  @Transform(({ value }) => new Date(value))
  birthday: Date;
  @IsArray()
  @IsString({ each: true })
  weaponIds: string[];
  @ValidateNested()
  attributes: Attributes;
  @IsString()
  keyAttribute: string;
}
