import { Transform } from 'class-transformer';
import {
  IsArray,
  IsDate,
  IsNumber,
  IsOptional,
  IsString,
  ValidateNested,
} from 'class-validator';

class Attributes {
  @IsNumber()
  @IsOptional()
  strength: number;
  @IsNumber()
  @IsOptional()
  dexterity: number;
  @IsNumber()
  @IsOptional()
  constitution: number;
  @IsNumber()
  @IsOptional()
  intelligence: number;
  @IsNumber()
  @IsOptional()
  wisdom: number;
  @IsNumber()
  @IsOptional()
  charisma: number;
}

export class UpdateKnightDto {
  @IsString()
  @IsOptional()
  name: string;
  @IsString()
  @IsOptional()
  nickname: string;
  @IsDate()
  @IsOptional()
  @Transform(({ value }) => new Date(value))
  birthday: Date;
  @IsArray()
  @IsOptional()
  @IsString({ each: true })
  weaponIds: string[];
  @ValidateNested()
  @IsOptional()
  attributes: Attributes;
  @IsString()
  @IsOptional()
  keyAttribute: string;
}
