import { PrismaService } from 'src/database/prisma.service';
import { Injectable } from '@nestjs/common';
import { WeaponModel } from './models/weapon.model';
import { Prisma } from '@prisma/client';

@Injectable()
export class WeaponRepository {
  constructor(private readonly prismaService: PrismaService) {}

  async get(id: string) {
    const weapon = await this.prismaService.weapon.findUnique({
      where: { id },
    });

    if (!weapon) return null;
    return new WeaponModel(weapon);
  }

  async findMany(ids: string[]) {
    const weapons = await this.prismaService.weapon.findMany({
      where: { id: { in: ids } },
    });
    return weapons.map((character) => new WeaponModel(character));
  }

  async create(data: Prisma.WeaponCreateInput) {
    const weapon = await this.prismaService.weapon.create({ data });
    return new WeaponModel(weapon);
  }

  async list() {
    const weapons = await this.prismaService.weapon.findMany();
    return weapons.map((character) => new WeaponModel(character));
  }
}
