import { Test, TestingModule } from '@nestjs/testing';
import { WeaponService } from './weapon.service';
import { PrismaModule } from 'src/database/prisma.module';
import { WeaponRepository } from './weapon.repository';
import { WeaponDataBuilder } from 'test/models/WeaponDataBuilder';

describe('WeaponService', () => {
  let service: WeaponService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [WeaponService],
      providers: [WeaponRepository],
      imports: [PrismaModule],
    }).compile();

    service = module.get<WeaponService>(WeaponService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should return an array of weapons', async () => {
    const weapons = [WeaponDataBuilder.aWeapon().build()];

    jest
      .spyOn(WeaponRepository.prototype, 'list')
      .mockResolvedValueOnce(weapons);

    expect(weapons).toEqual(weapons);
  });
});
