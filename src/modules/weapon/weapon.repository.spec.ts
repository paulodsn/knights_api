import { PrismaModule } from 'src/database/prisma.module';
import { WeaponRepository } from './weapon.repository';
import { Test, TestingModule } from '@nestjs/testing';
import { DeepMockProxy, mockDeep } from 'jest-mock-extended';
import { PrismaService } from 'src/database/prisma.service';
import { PrismaClient } from '@prisma/client';
import { WeaponDataBuilder } from 'test/models/WeaponDataBuilder';

describe('WeaponRepository', () => {
  let repository: WeaponRepository;
  let prisma: DeepMockProxy<PrismaClient>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [WeaponRepository],
      imports: [PrismaModule],
    })
      .overrideProvider(PrismaService)
      .useValue(mockDeep<PrismaClient>())
      .compile();

    repository = module.get<WeaponRepository>(WeaponRepository);
    prisma = module.get(PrismaService);
  });

  it('should be defined', () => {
    expect(repository).toBeDefined();
  });

  it('should return an array of weapons', async () => {
    const weapons = [WeaponDataBuilder.aWeapon().build()];
    prisma.weapon.findMany.mockResolvedValueOnce(weapons);

    const result = await repository.list();

    expect(result).toEqual(weapons);
  });

  it('should create a new weapon', async () => {
    const weapon = WeaponDataBuilder.aWeapon().build();
    prisma.weapon.create.mockResolvedValueOnce(weapon);

    const result = await repository.create(weapon);

    expect(result).toEqual(weapon);
  });

  it('should return a weapon', async () => {
    const weapon = WeaponDataBuilder.aWeapon().build();
    prisma.weapon.findUnique.mockResolvedValueOnce(weapon);

    const result = await repository.get(weapon.id);

    expect(result).toEqual(weapon);
  });

  it('should return null when weapon is not found', async () => {
    prisma.weapon.findUnique.mockResolvedValueOnce(null);

    const result = await repository.get('1');

    expect(result).toBeNull();
  });

  it('should return an array of weapons', async () => {
    const weapons = [WeaponDataBuilder.aWeapon().build()];
    prisma.weapon.findMany.mockResolvedValueOnce(weapons);

    const result = await repository.findMany([weapons[0].id]);

    expect(result).toEqual(weapons);
  });
});
