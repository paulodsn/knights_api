export class WeaponModel {
  id: string;
  name: string;
  mod: number;
  attr: string;
  equipped: boolean;

  constructor(data: WeaponModel) {
    Object.assign(this, data);
  }
}
