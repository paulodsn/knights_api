import { Test, TestingModule } from '@nestjs/testing';
import { WeaponController } from './weapon.controller';
import { WeaponService } from './weapon.service';
import { WeaponRepository } from './weapon.repository';
import { PrismaModule } from 'src/database/prisma.module';
import { WeaponDataBuilder } from 'test/models/WeaponDataBuilder';

describe('WeaponController', () => {
  let controller: WeaponController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [WeaponController],
      providers: [WeaponService, WeaponRepository],
      imports: [PrismaModule],
    }).compile();

    controller = module.get(WeaponController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should return an array of weapons', async () => {
    const weapons = [WeaponDataBuilder.aWeapon().build()];
    jest.spyOn(WeaponService.prototype, 'list').mockResolvedValueOnce(weapons);

    const result = await controller.list();

    expect(result).toEqual(weapons);
  });
});
