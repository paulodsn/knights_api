import { Module } from '@nestjs/common';
import { WeaponRepository } from './weapon.repository';
import { PrismaModule } from 'src/database/prisma.module';
import { WeaponService } from './weapon.service';
import { WeaponController } from './weapon.controller';

@Module({
  providers: [WeaponRepository, WeaponService],
  exports: [WeaponRepository],
  imports: [PrismaModule],
  controllers: [WeaponController],
})
export class WeaponModule {}
