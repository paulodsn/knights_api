import { Injectable } from '@nestjs/common';
import { WeaponRepository } from './weapon.repository';

@Injectable()
export class WeaponService {
  constructor(private readonly weaponRepository: WeaponRepository) {}

  async list() {
    return this.weaponRepository.list();
  }
}
