import { Controller, Get } from '@nestjs/common';
import { WeaponService } from './weapon.service';
import { ApiResponse, ApiTags } from '@nestjs/swagger';

@Controller('weapons')
export class WeaponController {
  constructor(private readonly weaponService: WeaponService) {}

  @Get()
  @ApiTags('weapons')
  @ApiResponse({ status: 200, description: 'List' })
  @ApiResponse({ status: 400, description: 'Bad request' })
  async list() {
    return this.weaponService.list();
  }
}
