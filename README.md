# Knights API

## Pré-Requisito

- Node 18+

## Como executar

1. Execute `pnpm install`
2. Copiar o arquivo `.env.example` para `.env`
3. Subir o mongoDB com docker: `docker compose up -d`
4. Executar seed: `npx prisma db seed`
5. Execute o comando `pnpm start:dev` para iniciar o projeto
6. pronto :)
